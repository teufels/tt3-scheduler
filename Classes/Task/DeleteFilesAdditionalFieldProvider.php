<?php

namespace Teufels\Tt3Scheduler\Task;

/**
 * This file is part of the "tt3_scheduler" Extension for TYPO3 CMS.
 * based on https://extensions.typo3.org/extension/deletefiles
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Scheduler\AbstractAdditionalFieldProvider;
use TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;
use TYPO3\CMS\Scheduler\SchedulerManagementAction;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

/**
 * Class DeleteFilesAdditionalFieldProvider.
 */
class DeleteFilesAdditionalFieldProvider extends AbstractAdditionalFieldProvider
{
    /**
     * @var LanguageService
     */
    protected $languageService;

    /**
     * BlackList of Directories
     *
     * @var string
     */
    protected $blockList = 'typo3,typo3conf,typo3_src,typo3temp,uploads';

    /**
     * Construct class.
     */
    public function __construct()
    {
        $this->languageService = $GLOBALS['LANG'];
    }

    /**
     * This method is used to define new fields for adding or editing a task
     *
     * @param array $taskInfo Reference to the array containing the info used in the add/edit form
     * @param AbstractTask|null $task When editing, reference to the current task. NULL when adding.
     * @param SchedulerModuleController $schedulerModule Reference to the calling object (Scheduler's BE module)
     * @return array Array containing all the information pertaining to the additional fields
     */
    public function getAdditionalFields(array &$taskInfo, $task, SchedulerModuleController $schedulerModule) {
        $currentSchedulerModuleAction = $schedulerModule->getCurrentAction();

        // process fields
        if (empty($taskInfo['deletefiles_directory'])) {
            if ($currentSchedulerModuleAction === SchedulerManagementAction::ADD) {
                $taskInfo['deletefiles_directory'] = '';
            } elseif ($currentSchedulerModuleAction === SchedulerManagementAction::EDIT) {
                $taskInfo['deletefiles_directory'] = $task->deletefiles_directory;
            } else {
                $taskInfo['deletefiles_directory'] = '';
            }
        }

        if (empty($taskInfo['deletefiles_time'])) {
            if ($currentSchedulerModuleAction === SchedulerManagementAction::ADD) {
                $taskInfo['deletefiles_time'] = [];
            } elseif ($currentSchedulerModuleAction === SchedulerManagementAction::EDIT) {
                $taskInfo['deletefiles_time'] = $task->deletefiles_time;
            } else {
                $taskInfo['deletefiles_time'] = '';
            }
        }

        if (empty($taskInfo['deletefiles_method'])) {
            if ($currentSchedulerModuleAction === SchedulerManagementAction::ADD) {
                $taskInfo['deletefiles_method'] = 0;
            } elseif ($currentSchedulerModuleAction === SchedulerManagementAction::EDIT) {
                $taskInfo['deletefiles_method'] = $task->deletefiles_method;
            } else {
                $taskInfo['deletefiles_method'] = 0;
            }
        }

        if (!isset($taskInfo['deletefiles_advancedMode']) || empty($taskInfo['deletefiles_advancedMode'])) {
            if ($currentSchedulerModuleAction === SchedulerManagementAction::ADD) {
                $taskInfo['deletefiles_advancedMode'] = false;
            } elseif ($currentSchedulerModuleAction === SchedulerManagementAction::EDIT) {
                $taskInfo['deletefiles_advancedMode'] = (bool)$task->deletefiles_advancedMode;
            } else {
                $taskInfo['deletefiles_advancedMode'] = false;
            }
        }

        $additionalFields = [];

        // render directory field
        $fieldId = 'task_deletefiles_directory';
        $fieldCode = '<input type="text" class="form-control" name="tx_scheduler[deletefiles_directory]" id="'.$fieldId.'" value="'.
            htmlspecialchars($taskInfo['deletefiles_directory']).'" size="30">';

        $additionalFields[$fieldId] = [
            'code' => $fieldCode,
            'label' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:addfields_label_directory',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldId,
        ];

        // render time field
        $fieldId = 'task_deletefiles_time';
        $fieldCode = '<select name="tx_scheduler[deletefiles_time]" id="'.$fieldId.'" class="form-select">';

        $fieldValueArray = $this->getTimes();
        foreach ($fieldValueArray as $time => $label) {
            $fieldCode .= "\t".'<option value="'.htmlspecialchars($time).'"'.
                (($time == $taskInfo['deletefiles_time']) ? ' selected="selected"' : '').
                '>'.$label.'</option>';
        }

        $fieldCode .= '</select>';

        $additionalFields[$fieldId] = [
            'code' => $fieldCode,
            'label' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:addfields_label_time',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldId,
        ];

        // render method field
        $fieldId = 'task_deletefiles_method';
        $fieldCode = '<select name="tx_scheduler[deletefiles_method]" id="'.$fieldId.'" class="form-select">';

        $fieldValueArray = $this->getMethods();
        foreach ($fieldValueArray as $method => $label) {
            $fieldCode .= "\t".'<option value="'.htmlspecialchars($method).'"'.
                (($method == $taskInfo['deletefiles_method']) ? ' selected="selected"' : '').'>'.$label.'</option>';
        }

        $fieldCode .= '</select>';

        $additionalFields[$fieldId] = [
            'code' => $fieldCode,
            'label' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:addfields_label_method',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldId,
        ];

        // render advancedMode field
        $fieldId = 'task_deletefiles_advancedMode';
        if($taskInfo['deletefiles_advancedMode']) {
            $advancedMode_checked =  'checked="checked" ';
        } else {
            $advancedMode_checked =  '';
        }
        $additionalFields[$fieldId] = [
            'code' => '<input type="checkbox" name="tx_scheduler[deletefiles_advancedMode]" ' . $advancedMode_checked . '  />',
            'label' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:addfields_label_advancedMode',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldId,
        ];

        return $additionalFields;
    }

    /**
     * @todo add a hook here
     *
     * @return array
     */
    public function getTimes()
    {
        return [
            '0' => $this->translate('addfields_time_all'),
            '1h' => '1 '.$this->translate('addfields_time_h'),
            '6h' => '6 '.$this->translate('addfields_time_h'),
            '12h' => '12 '.$this->translate('addfields_time_h'),
            '24h' => '24 '.$this->translate('addfields_time_h'),
            '48h' => '48 '.$this->translate('addfields_time_h'),
            '72h' => '72 '.$this->translate('addfields_time_h'),
            '7d' => '7 '.$this->translate('addfields_time_d'),
            '14d' => '14 '.$this->translate('addfields_time_d'),
            '1m' => '1 '.$this->translate('addfields_time_m'),
            '3m' => '3 '.$this->translate('addfields_time_m'),
            '6m' => '6 '.$this->translate('addfields_time_m'),
            '12m' => '12 '.$this->translate('addfields_time_m'),
        ];
    }

    /**
     * @todo add a hook here
     *
     * @return array
     */
    public function getMethods()
    {
        return [
            'delete_directory' => $this->translate('addfields_method_delete_directory'),
            'delete_files' => $this->translate('addfields_method_delete_files'),
            'delete_directories' => $this->translate('addfields_method_delete_directories'),
            'delete_all' => $this->translate('addfields_method_delete_all'),
            'delete_all_recursive' => $this->translate('addfields_method_delete_all_recursive'),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function validateAdditionalFields(
        array &$submittedData,
        SchedulerModuleController $schedulerModule
    ) {
        $validInput = true;

        // clean data
        if(isset($submittedData['deletefiles_advancedMode']) && $submittedData['deletefiles_advancedMode']) {
            //advancedMode: leading slash for absolute path allowed
            $submittedData['deletefiles_directory'] = rtrim($submittedData['deletefiles_directory'], '/');
        } else {
            $submittedData['deletefiles_directory'] = trim($submittedData['deletefiles_directory'], '/');
        }

        $submittedData['deletefiles_time'] = trim($submittedData['deletefiles_time']);

        $path = $submittedData['deletefiles_directory'];
        $publicPath = Environment::getPublicPath().'/';

        if (!$this->isPathValid($path, $submittedData)) {
            if (!(strlen($path) > 0 && is_dir($publicPath.$path) && GeneralUtility::isAllowedAbsPath($publicPath.$path))) {
                // @extensionScannerIgnoreLine
                $this->addMessage(
                    sprintf($this->translate('addfields_notice_path_invalid'), $path),
                    ContextualFeedbackSeverity::ERROR
                );
                $validInput = false;
            }
        }

        return $validInput;
    }

    /**
     * {@inheritdoc}
     */
    public function saveAdditionalFields(array $submittedData, AbstractTask $task)
    {
        $task->deletefiles_directory = $submittedData['deletefiles_directory'];
        $task->deletefiles_time = $submittedData['deletefiles_time'];
        $task->deletefiles_method = $submittedData['deletefiles_method'];
        if(isset($submittedData['deletefiles_advancedMode'])) {
            $task->deletefiles_advancedMode = (bool)$submittedData['deletefiles_advancedMode'];
        } else {
            $task->deletefiles_advancedMode = false;
        }
    }

    /**
     * validate if given path is valid.
     *
     * @return bool
     */
    public function isPathValid($path, $submittedData): bool
    {
        $path = trim($path, DIRECTORY_SEPARATOR);
        if(isset($submittedData['deletefiles_advancedMode']) && $submittedData['deletefiles_advancedMode']) {
            return GeneralUtility::validPathStr($path);
        }
        $publicPath = Environment::getPublicPath().'/';
        return strlen($path) > 0
            && file_exists($publicPath . $path)
            && GeneralUtility::isAllowedAbsPath($publicPath . $path)
            && GeneralUtility::validPathStr($path)
            && !GeneralUtility::inList($this->blockList, $path)
            ;
    }

    /**
     * Translate by key.
     *
     * @param string $key
     * @param string $prefix
     *
     * @return string
     */
    protected function translate($key, $prefix = 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xml:')
    {
        return $this->languageService->sL($prefix.$key);
    }
}
