[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--scheduler-orange.svg)](https://bitbucket.org/teufels/tt3s-scheduler/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__scheduler-red.svg)](https://bitbucket.org/teufels/tt3-scheduler/src/main/)
![version](https://img.shields.io/badge/version-2.0.*-yellow.svg?style=flat-square)

[ ṯeufels ] Scheduler
==========
provide scheduler tasks

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-scheduler`

***

### Requirements
- none

***

### How to use
- Install with composer

***

### Migration
- replace `beewilly/hive_schedule` with `teufels/tt3-scheduler`
- migrate old hive_scheduler Task to new tt3_scheduler Task

***

### Changelog
- 2.0.0 version for TYPO3 v13
- 1.0.3 Fix Deprecation: #97787 - Severities of flash messages and reports deprecated
- 1.0.2 LogFileHandlingTask: fix delete of old files
- 1.0.1 use defaultMailFromAddress as Sender & labels
- 1.0.0 intial from [hive_scheduler](https://bitbucket.org/teufels/hive_scheduler/src/main/)