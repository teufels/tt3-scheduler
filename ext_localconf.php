<?php
defined('TYPO3') or die('Access denied.');

/**
 * Registering class to scheduler
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Teufels\Tt3Scheduler\Task\LogFileHandlingTask::class] = [
    'extension' => 'tt3_scheduler',
    'title' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:localconf_logFileHandling_title',
    'description' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:localconf_logFileHandling_description',
    'additionalFields' => \Teufels\Tt3Scheduler\Task\LogFileHandlingAdditionalFieldProvider::class,
];

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Teufels\Tt3Scheduler\Task\DeleteFilesTask::class] = [
    'extension' => 'tt3_scheduler',
    'title' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:localconf_deleteFiles_title',
    'description' => 'LLL:EXT:tt3_scheduler/Resources/Private/Language/locallang.xlf:localconf_deleteFiles_description',
    'additionalFields' => \Teufels\Tt3Scheduler\Task\DeleteFilesAdditionalFieldProvider::class,
];
